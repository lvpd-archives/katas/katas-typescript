import Big from 'big.js';

export default class Category {
    private _name: string;
    private _taxPercentage: Big;

    constructor() {}

    get name(): string {
        return this._name;
    }

    set name(name: string) {
        this._name = name;
    }


    get taxPercentage(): Big {
        return this._taxPercentage;
    }

    set taxPercentage(taxPercentage: Big) {
        this._taxPercentage = taxPercentage;
    }
}
