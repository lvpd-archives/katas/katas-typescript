import Product from "./Product";
import Big from "big.js";

export default class OrderItem {
    private _product: Product;
    private _quantity: number;
    private _taxedAmount: Big;
    private _tax: Big;

    constructor() {}

    get product(): Product {
        return this._product;
    }

    set product(value: Product) {
        this._product = value;
    }

    get quantity(): number {
        return this._quantity;
    }

    set quantity(value: number) {
        this._quantity = value;
    }

    get taxedAmount(): Big {
        return this._taxedAmount;
    }

    set taxedAmount(value: Big) {
        this._taxedAmount = value;
    }

    get tax(): Big {
        return this._tax;
    }

    set tax(value: Big) {
        this._tax = value;
    }
}
