import Category from "./Category";
import Big from "big.js";

export default class Product {
    private _product: Product;
    private _name: string;
    private _price: Big;
    private _category: Category;

    constructor() {}

    get product(): Product {
        return this._product;
    }

    set product(value: Product) {
        this._product = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get price(): Big {
        return this._price;
    }

    set price(value: Big) {
        this._price = value;
    }

    get category(): Category {
        return this._category;
    }

    set category(value: Category) {
        this._category = value;
    }
}
