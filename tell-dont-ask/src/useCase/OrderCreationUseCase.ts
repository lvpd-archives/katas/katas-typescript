import OrderRepository from "../repository/OrderRepository";
import ProductCatalog from "../repository/ProductCatalog";
import SellItemsRequest from "./SellItemsRequest";
import Order from "../domain/Order";
import UnknownProductException from "./UnknownProductException";
import OrderItem from "../domain/OrderItem";
import OrderStatus from "../domain/OrderStatus";
import Big from "big.js";

export default class OrderCreationUseCase {
    private readonly _orderRepository: OrderRepository;
    private readonly _productCatalog: ProductCatalog;

    constructor(orderRepository: OrderRepository, productCatalog: ProductCatalog) {
        this._orderRepository = orderRepository;
        this._productCatalog = productCatalog;
    }

    public run(request: SellItemsRequest): void {
        let order = new Order();
        order.status = OrderStatus.CREATED;
        order.items = [];
        order.currency = "EUR";
        order.total = new Big("0.00");
        order.tax = new Big("0.00");

        for (let itemRequest of request.requests) {
            let product = this._productCatalog.getByName(itemRequest.productName);

            if (product === undefined) {
                throw new UnknownProductException()
            } else {
                const unitaryTax = product.price.div(new Big(100)).mul(product.category.taxPercentage).round(2);
                const unitaryTaxedAmount = product.price.add(unitaryTax).round(2);
                const taxedAmount = unitaryTaxedAmount.mul(new Big(itemRequest.quantity)).round(2);
                const taxAmount = unitaryTax.mul(new Big(itemRequest.quantity));

                let orderItem = new OrderItem();
                orderItem.product = product;
                orderItem.quantity = itemRequest.quantity;
                orderItem.tax = taxAmount;
                orderItem.taxedAmount = taxedAmount;
                order.items.push(orderItem);

                order.total = order.total.add(taxedAmount);
                order.tax = order.tax.add(taxAmount);
            }
        }
        this._orderRepository.save(order);
    }
}
